import glob
import os
from typing import Tuple

import mrcfile
import numpy as np
import pyfftw

from .base import DATASET_REGISTRY, BaseDataset


@DATASET_REGISTRY.register()
class SyntheticNoiseDataset(BaseDataset):
    """
    A :class:`BaseDataset` that reads multiple tomograms and returns a random slice of
    each tomogram along with a corrupted version of that slice as the training target.
    This dataset includes all the tomogram map files (`.mrc`) located in either
    `train_dir` or `validation_dir` depending on whether the `is_train` flag is set. The
    size of each slice is determined by the given `patch_size`, and each slice is
    corrupted by randomly removing some high-frequency components in the spectrum. For
    more information, please refer to the [noise2noise](https://arxiv.org/pdf/1803.04189)
    paper on how this method works.
    """

    def __init__(
        self,
        *,
        train_dir: str,
        validation_dir: str,
        normalise: bool = True,
        p_at_edge: float = 0.025,
        patch_size: int = 64,
        is_train: bool = True,
    ) -> BaseDataset:
        """
        :param str train_dir: Directory containing the training tomogram map files.
        :param str validation_dir: Directory containing the validation tomogram map
            files.
        :param bool normalise: If True, normalise the slices (default: True).
        :param float p_at_edge: Probability of retaining the frequency components at the
            edges (default: 0.025).
        :param int patch_size: Size of each slice (default: 64).
        :param bool is_train: If True, use the training directory; otherwise, use the
            validation directory (default: True).
        :return BaseDataset: The initialized dataset object.
        """
        super().__init__()
        self.is_train = is_train
        self.files = glob.glob(
            os.path.join(train_dir if is_train else validation_dir, "*.mrc")
        )
        self.normalise = normalise
        self.p_at_edge = p_at_edge
        self.patch_size = patch_size

    def __len__(self) -> int:
        """
        :return int: The total number of slices
        """
        return len(self.files)

    @staticmethod
    def _normalise(x: np.ndarray, eps: float = 1e-8):
        """
        Normalise the given array `x` by subtracting the mean and dividing by its
        standard deviation.

        :param np.ndarray x: The input array to be normalised.
        :param float eps: A small epsilon value to prevent division by zero,
            defaults to 1e-8.
        :return np.ndarray: The normalised array.
        """
        mean = x.mean()
        std = x.std()
        return (x - mean) / max(std, eps)

    def _corrupt_data(self, arr: np.ndarray) -> np.ndarray:
        """
        Randomly removes some components of the given array `arr`, weighted towards the
        high-frequency components. The fraction of components removed depends on the
        `p_at_edge` parameter. Please refer to the
        [noise2noise](https://arxiv.org/pdf/1803.04189) paper for more details.

        :param np.ndarray arr: The input array to be corrupted.
        :return np.ndarray: The corrupted array.
        """
        spc = pyfftw.interfaces.numpy_fft.fftn(arr).astype(np.complex64)
        spc = pyfftw.interfaces.numpy_fft.fftshift(spc)

        r = np.sqrt(
            sum(
                [
                    np.expand_dims(
                        np.square(np.arange(x, dtype=np.float32) - x // 2),
                        axis=tuple(set(range(3)) - {i}),
                    )
                    for i, x in enumerate(spc.shape)
                ]
            )
        )
        m = self.p_at_edge ** (1 / (max(spc.shape) // 2)) ** r

        k = np.random.uniform(0.0, 1.0, size=spc.shape) ** 2 < m
        k = k & k[::-1, ::-1, ::-1]
        sval = spc * k
        spc = pyfftw.interfaces.numpy_fft.ifftshift(sval / (m + ~k))
        arr = np.real(pyfftw.interfaces.numpy_fft.ifftn(spc)).astype(np.float32)
        return arr

    def __getitem__(self, index: int) -> Tuple[np.ndarray, np.ndarray]:
        """
        Retrieves the tomogram at the given `index`. Then, randomly selects a slice of
        the tomogram and applies random rotation as augmentation if the `is_train` flag
        is set. The selected slice is then corrupted using the `_corrupt_data` method,
        which randomly removes some frequency components (weighted towards
        high-frequency components) to create the training target. Finally, returns the
        pair for training.

        :param int index: The index of the tomogram to retrieve.
        :return Tuple[np.ndarray, np.ndarray]: A tuple containing the original slice and
            its corrupted version.
        """
        x = np.array(mrcfile.read(self.files[index]), dtype=np.float32, copy=True)
        if self.normalise:
            x = self._normalise(x)

        p = []
        if self.is_train:
            for s in x.shape:
                i = np.random.randint(0, s - self.patch_size)
                p.append(slice(i, i + self.patch_size))
        else:
            for s in x.shape:
                i = (s - self.patch_size) // 2
                p.append(slice(i, i + self.patch_size))

        x = x[tuple(p)].astype(np.float32)

        if np.random.random() < 0.5 and self.is_train:
            k = np.random.randint(0, 4)
            x = np.rot90(x, k=k, axes=[0, 2])

        y = self._corrupt_data(x) if self.is_train else x
        return x[np.newaxis].copy(), y[np.newaxis].copy()
