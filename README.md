# CCPEM Denoiser

Model training and denoising codes for cryo-electron tomography data using [PyTorch](https://pytorch.org/).

## Installation

**OPTIONAL**: We encourage you to set up a new [conda](https://conda.io/projects/conda/en/latest/user-guide/getting-started.html) environment to work on this project.
```bash
conda create -n ccpem-denoiser python=3.8
conda activate ccpem-denoiser
```

### Requirements
- `Python >= 3.8`
- `PyTorch >= 2.0`

**NOTE**: Please install `torch>=2.0` by following the instructions on the [official site](https://pytorch.org/get-started/locally/).

Then, clone this repository and install all the dependencies:
```bash
git clone <github-repo-url>/ccpem-denoiser
cd ccpem-denoiser
pip install .
```

## Pre-trained Model

A model pre-trained on 200 tomograms from the [Cryo-ET Data Portal](https://cryoetdataportal.czscience.com/) using the `SyntheticNoiseDataset` as the dataset is available for download at [`model.tar.gz`](https://gitlab.com/dingsheng-ong/denoiser-weights/-/raw/main/model.tar.gz?ref_type=heads&inline=false). This compressed file includes the model weights (`model.pth`) and the configuration file (`config.yaml`) required for running denoising. To use the pre-trained model for denoising your samples, download the [`model.tar.gz`](https://gitlab.com/dingsheng-ong/denoiser-weights/-/raw/main/model.tar.gz?ref_type=heads&inline=false) file to this project directory and extract the contents of the file: `tar xzvf model.tar.gz`. Then, run the script to denoise your sample:
```bash
python run.py --config-file config.yaml denoise.maps ./path-to-input.mrc denoise.output ./path-to-save-output.mrc
``` 

## TL;DR

To start the program, you would use the following command in your terminal:
```bash
python run.py --config-file config.yaml
```
If you need to overwrite any configurations when running the program, you can append the parameters at the end of the command like this:
```bash
python run.py --config-file config.yaml train.output_dir ./model/unet/
```

To use the program for denoising without training the model, you have two options, i) add the `--denoise-only` flag when running the program:
```bash
python run.py --denoise-only --config-file config.yaml
```
, or remove the `train` section from the `config.yaml` file as shown in this [example](#example-denoise-only).

Depending on your use case, you'll need a different configuration written in `config.yaml`.

### Multi-GPUs

Using multiple GPUs to run the program is straightforward. Simply start the `run.py` script using `torchrun` instead of `python`. The example below demonstrates running the program using **4** GPUs:
```bash
torchrun --standalone --nnodes=1 --nproc-per-node=4 run.py --config-file config.yaml
```

### Example `config.yaml` for Different Use Cases

1. Training and Denoising Half Maps of One Tomogram Using U-Net:
```yaml
data:
  name: HalfMapsDataset
  half_maps:
    - [./path-to-half-1.mrc, ./path-to-half-2.mrc]
  nb_slices: 1500
  train_val_split: 0.8
  normalise: true
  patch_size: 64
model:
  name: UNet3d
train:
  batch_size: 16
  epochs: 200
  output_dir: model
  optimizer:
    name: Adam
    lr: 4.0e-4
  scheduler:
    name: ReduceLROnPlateau
    factor: 0.5
    threshold: 0
denoise:
  maps: [./path-to-half-1.mrc, ./path-to-half-2.mrc]
  model_path: model/model_best.pth
  normalise: true
  overlap_tile: true
  patch_size: 64
  batch_size: 16
  overlap_size: 4
  output: model/denoised.mrc
```

2. <a name="example-denoise-only" id="example-denoise-only"></a>Denoising a Single Map Using a Pre-trained Model:
```yaml
model:
  name: UNet3d
denoise:
  maps: ./path-to-map.mrc
  model_path: ./path-to-model.pth
  normalise: true
  overlap_tile: true
  patch_size: 64
  batch_size: 16
  overlap_size: 4
  output: ./path-to-denoised-map.mrc
```

3. Training on Multiple Tomograms Using 3D VAE Model with Custom Configurations:
```yaml
data:
  name: SyntheticNoiseDataset
  train_dir: ./path-to-train-dir/
  validation_dir: ./path-to-validation-dir/
  normalise: true
  p_at_edge: 0.025
  patch_size: 64
model:
  name: VAE3d
  nch_feats: 64
  latent_dim: 256
  ksize: 5 # larger kernel size
  n_depth: 3 # deeper model
train:
  batch_size: 16
  epochs: 200
  output_dir: model
  optimizer:
    name: Adam
    lr: 4.0e-4
  scheduler:
    name: ReduceLROnPlateau
    factor: 0.5
    threshold: 0
```

### Denoising Multiple Tomograms

To denoise multiple tomograms, start by configuring your `config.yaml` file as previously shown. Leave the `denoise.maps` and `denoise.output` fields blank while ensuring that `model_path` is defined.
```yaml
model:
  name: UNet3d
denoise:
  maps: null
  model_path: ./path-to-model.pth
  normalise: true
  overlap_tile: true
  patch_size: 64
  batch_size: 16
  overlap_size: 4
  output: null
```
Then, pass in the input and output when running the program:
```bash
python run.py --denoise-only --config-file config.yaml denoise.maps ./input/data.mrc denoise.output ./output/data.mrc
```
To run multiple maps at once, you can use the following bash command:
```bash
ls input/ | xargs -I {} python run.py --denoise-only --config-file config.yaml denoise.maps ./input/{} denoise.output ./output/{}
```

## Getting Started

### Configuration

Before training or running denoising on your data, you must write your configuration file (`config.yaml`). There are four sections in the configuration you need to define for training, and only two (`model` and `denoise`) if you only want to denoise your data using pre-trained models.
```yaml
data:
    ...
model:
    ...
train:
    ...
denoise:
    ...
```

The first part is the `data` section, which defines all the specifications for training data. There are currently two types of training data implemented in `sr/data`: `HalfMapsDataset` and `SyntheticNoiseDataset`. To use the implemented dataset, simply put the name of the dataset in the `data.name` field, and the arguments to initialize the dataset object in `data.key: value`. For example, the `HalfMapsDataset` with the following `__init__` method can be defined as follows:
```python
def __init__(
  self,
  *,
  half_maps: List[List[str]],
  nb_slices: int = 1500,
  train_val_split: float = 0.8,
  normalise: bool = True,
  patch_size: Union[Tuple[int, int, int], int] = 64,
  is_train: bool = True,
):
```
This can be configured in config.yaml like so:
```yaml
data:
  name: HalfMapsDataset
  half_maps:
    - [./path-to-half-1.mrc, ./path-to-half-2.mrc]
  nb_slices: 1500
  train_val_split: 0.8
  normalise: true
  patch_size: 64
```

Likewise, to define the denoiser model architecture, you can write the specifications under the `model` section. Similar to the dataset, the model can be identified by `model.name` and the parameters to initialize the model are written in `model.key: value` form. There are currently three models implemented: `UNet3d`, `VAE3d`, and `VQVAE3d`. The model parameters can be found in the `__init__` method of the respective model class, written in `src/model/`. For example, the parameters of `UNet3d` can be defined as follows:
```yaml
model:
  name: UNet3d
  nch_in: 1
  nch_out: 1
  nch_feats: 32
  ksize: 3
  n_depth: 2
  bias: true
  residual: false
```

The next section, `train`, defines the training hyperparameters such as batch size and learning rate. The following shows the complete list of training configurations:
```yaml
train:
  device: cuda
  batch_size: 16
  num_workers: 1
  epochs: 200
  output_dir: model/
  ckpt_freq: null
  rng_seed: 0
  optimizer:
    name: Adam
    lr: 4.0e-4
  scheduler:
    name: ReduceLROnPlateau
    factor: 0.5
    threshold: 0
```
The `name` in `optimizer` and `scheduler` can be changed to other optimizers available in `torch.optim` such as `SGD` and `LBFGS`, and the parameters can be defined under the `train.optimizer` section, likewise for `scheduler`. If no scheduler is defined, a learning scheduler will not be used in the training.

Finally, the `denoise` section includes the data needed for denoising samples. The denoising process uses an overlap-tile strategy for a scalable solution for large tomograms, and the parameters can be set in the fields: `denoise.patch_size`, `denoise.batch_size`, and `denoise.overlap_size`. The `denoise.maps` is the path to the map that you want to denoise. Currently, only a **single map** or **a pair of half maps** are supported. The denoising also requires `model_path` to select the trained model that you want to use for denoising; note that the model weight must be compatible with the model architecture defined in the `model` section. Then, the denoised map is output to the given `denoise.output`. The complete `denoise` configurations are given below:
```yaml
denoise:
  maps: ./path-to-map.mrc
  # OR for half-maps
  maps: [./path-to-half1.mrc, ./path-to-half2.mrc]
  model_path: ./path-to-model.pth
  normalise: true
  overlap_tile: true
  patch_size: 64
  batch_size: 16
  overlap_size: 4
  output: ./path-to-denoised-map.mrc
```
By default, the denoising process employs the overlap-tile strategy to handle large tomograms, breaking them into smaller slices for batch processing. To disable this strategy, either remove the related fields (`overlap_tile`, `patch_size`, `batch_size`, `overlap_size`) from the configuration or set `overlap_tile` to `false`.

## Customisations

You can extend the functionality of the program by adding your own dataset and model. Here's how:

### Custom Dataset
To introduce your dataset, create a new dataset file in `src/data`, such as `src/data/custom.py`. Below is an example showing how to add a new dataset that reads all `.mrc` files and returns a copy of itself as the training target.

`src/data/custom.py`
```python
import glob
import os

import mrcfile
import numpy as np

from .base import DATASET_REGISTRY, BaseDataset


@DATASET_REGISTRY.register()
class CustomDataset(BaseDataset):
  def __init__(self, data_dir, is_train=True):
    super().__init__()
    self.mrcfiles = glob.glob(os.path.join(data_dir, "*.mrc"))
  
  def __len__(self):
    return len(self.mrcfiles)
  
  def __getitem__(self, index):
    array = np.array(mrcfile.read(self.mrcfiles[index]), dtype=np.float32, copy=True)
    return array[np.newaxis], array[np.newaxis]
```
Then, import your dataset in `src/data/__init__.py`:
```python
from .base import DATASET_REGISTRY, build_loaders
from .half_maps import HalfMapsDataset
from .synthetic_noise import SyntheticNoiseDataset
# import your dataset here
from .custom import CustomDataset

__all__ = [k for k in globals().keys() if not k.startswith("_")]

```
To use the dataset, define the configurations in config.yaml:
`config.yaml`:
```yaml
data:
  name: CustomDataset
  data_dir: ./path-to-mrc-files/
```

### Custom Model

Adding a custom model follows similar steps. Begin by writing your model file in `src/model`:

`src/model/custom.py`
```python
from torch import nn
from torch.nn import functional as F

from .base import MODEL_REGISTRY, BaseModel


@MODEL_REGISTRY.register()
class CustomModel(BaseModel):
  def __init__(self, kernel_size):
    super().__init__()
    self.conv = nn.Conv3d(1, 1, kernel_size, 1, kernel_size // 2, bias=True)
  
  def forward(self, input, target=None):
    output = self.conv(input)
    if target is not None:
      return output, F.mse_loss(output, target)
    else:
      return output
```
Next, import the model in `src/model/__init__.py`:
```python
from .base import MODEL_REGISTRY, build_model
from .unet import UNet3d
from .vae import VAE3d
from .vqvae import VQVAE3d
# import your model here
from .custom import CustomModel

__all__ = [k for k in globals().keys() if not k.startswith("_")]

```
Finally, employ your model by configuring `config.yaml`:
```yaml
model:
  name: CustomModel
  kernel_size: 3
```

## References
- https://github.com/NVlabs/noise2noise
- https://github.com/juglab/cryoCARE_pip
- https://cryoetdataportal.czscience.com/