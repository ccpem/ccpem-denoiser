from typing import Tuple, Union

import torch
from torch import nn
from torch.nn import functional as F

from .base import MODEL_REGISTRY, BaseModel

__all__ = ["VQVAE3d"]


class VectorQuantizer(nn.Module):
    """Implement the Vector Quantizer module."""

    def __init__(self, n_embedding: int, latent_dim: int, beta: float = 0.25):
        """
        :param int n_embedding: Number of embeddings.
        :param int latent_dim: Dimension of each embedding.
        :param float beta: Scaling factor for commitment loss (default: 0.25).
        """
        super().__init__()
        self.embeddings = nn.Embedding(n_embedding, latent_dim)
        self.beta = beta
        self._dim = latent_dim

    def forward(self, x: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        """Sample the nearest embeddings to the given representation, `x`, and
        return the commitment loss and the vector quantization loss.

        :param torch.Tensor x: The input representation.
        :return Tuple[torch.Tensor, torch.Tensor]: Sampled embeddings, sum of
            both losses.
        """

        z = x.permute(0, 2, 3, 4, 1).contiguous()
        s = z.shape
        z = z.view(-1, self._dim)

        d = (
            torch.sum(z * z, dim=1, keepdim=True)
            + torch.sum(self.embeddings.weight**2, dim=1)
            - 2 * torch.matmul(z, self.embeddings.weight.T)
        )
        qz = self.embeddings(d.argmin(dim=1)).view(s)
        qz = qz.permute(0, 4, 1, 2, 3).contiguous()
        qx = x + (qz - x).detach()

        # commitment loss + vq objective
        loss = self.beta * F.mse_loss(qz.detach(), x) + F.mse_loss(qz, x.detach())
        return qx, loss


@MODEL_REGISTRY.register()
class VQVAE3d(BaseModel):
    """Implement the 3D VQ-VAE."""

    def __init__(
        self,
        nch_in: int = 1,
        nch_out: int = 1,
        latent_dim: int = 64,
        beta: float = 0.25,
        n_embedding: int = 512,
        nch_feats: int = 32,
        ksize: int = 3,
        n_depth: int = 2,
        bias: bool = True,
    ) -> BaseModel:
        """
        :param int nch_in: Number of input channels.
        :param int nch_out: Number of output channels.
        :param int latent_dim: Dimension of each embedding.
        :param float beta: Scaling factor for commitment loss (default: 0.25).
        :param int n_embedding: Number of embeddings (default: 512).
        :param int nch_feats: Number of channels in the first convolution layer
            (default: 32).
        :param int ksize: Convolution kernel size (default: 3).
        :param int n_depth: Number of convolution blocks, each reducing the
            input size by a factor of 2^n_depth (default: 2).
        :param bool bias: Whether to add bias in convolution operations
            (default: True).
        """
        super().__init__()
        self.beta = beta
        self.n_depth = n_depth

        c = nch_feats
        pad = ksize >> 1
        self.enc_0a = nn.Conv3d(nch_in, nch_feats, ksize, 1, pad, bias=bias)
        for i in range(n_depth + 1):
            if i > 0:
                self.register_module(
                    f"enc_{i}a", nn.Conv3d(c, c << 1, ksize, 1, pad, bias=bias)
                )
                c = c << 1
            self.register_module(
                f"enc_{i}b",
                nn.Conv3d(c, c if i < n_depth else c >> 1, ksize, 1, pad, bias=bias),
            )

        self.vq_in = nn.Conv3d(c >> 1, latent_dim, 1, 1, 0, bias=bias)
        self.vq_layer = VectorQuantizer(n_embedding, latent_dim, beta)
        self.vq_out = nn.Conv3d(latent_dim, c >> 1, 1, 1, 0, bias=bias)

        for i in reversed(range(n_depth)):
            self.register_module(
                f"dec_{i}a", nn.Conv3d(c, c >> 1, ksize, 1, pad, bias=bias)
            )
            c = c >> 1
            self.register_module(
                f"dec_{i}b",
                nn.Conv3d(c, c >> 1 if i > 0 else c, ksize, 1, pad, bias=bias),
            )

        self.conv = nn.Conv3d(nch_feats, nch_out, ksize, 1, pad, bias=bias)
        self.max_pooling = nn.MaxPool3d(kernel_size=2)
        self.upsample = lambda x, y: F.interpolate(x, y.shape[2:], mode="nearest")

    def forward(
        self, input: torch.Tensor, target: torch.Tensor = None
    ) -> Union[Tuple[torch.Tensor, torch.Tensor], torch.Tensor]:
        """See :class:`src.model.base.BaseModel`."""
        x = input
        skip = [x]
        for i in range(self.n_depth):
            x = F.relu_(self.get_submodule(f"enc_{i}a")(x))
            x = F.relu_(self.get_submodule(f"enc_{i}b")(x))
            skip.append(x)
            x = self.max_pooling(x)

        x = F.relu_(self.get_submodule(f"enc_{self.n_depth}a")(x))
        x = F.relu_(self.get_submodule(f"enc_{self.n_depth}b")(x))

        x = self.vq_in(x)
        x, vq_loss = self.vq_layer(x)
        x = self.vq_out(x)

        for i in reversed(range(self.n_depth)):
            h = skip.pop()
            x = self.upsample(x, h)
            x = torch.cat([x, h], dim=1)
            x = F.relu_(self.get_submodule(f"dec_{i}a")(x))
            x = F.relu_(self.get_submodule(f"dec_{i}b")(x))

        output = self.conv(x)

        if target is not None:
            return output, F.mse_loss(output, target) + vq_loss
        else:
            return output
