from typing import Tuple, Union

import torch
from torch import nn
from torch.nn import functional as F

from .base import MODEL_REGISTRY, BaseModel


@MODEL_REGISTRY.register()
class UNet3d(BaseModel):
    """Implement the 3D U-Net model."""

    def __init__(
        self,
        nch_in: int = 1,
        nch_out: int = 1,
        nch_feats: int = 32,
        ksize: int = 3,
        n_depth: int = 2,
        bias: bool = True,
        residual: bool = False,
    ):
        """
        :param int nch_in: Number of input channels (default: 1).
        :param int nch_out: Number of output channels (default: 1).
        :param int nch_feats: Number of channels in the first convolution layer
            (default: 32).
        :param int ksize: Size of the convolution kernel (default: 3).
        :param int n_depth: Number of convolution blocks, each block halves the input
            size (default: 2).
        :param bool bias: If True, adds bias to the convolution operations
            (default: True).
        :param bool residual: If True, adds a skip connection from input to output
            (default: False).
        """
        super().__init__()
        self.n_depth = n_depth
        self.residual = residual

        c = nch_feats
        pad = ksize >> 1
        self.enc_0a = nn.Conv3d(nch_in, nch_feats, ksize, 1, pad, bias=bias)
        for i in range(n_depth + 1):
            if i > 0:
                self.register_module(
                    f"enc_{i}a", nn.Conv3d(c, c << 1, ksize, 1, pad, bias=bias)
                )
                c = c << 1
            self.register_module(
                f"enc_{i}b",
                nn.Conv3d(c, c if i < n_depth else c >> 1, ksize, 1, pad, bias=bias),
            )

        for i in reversed(range(n_depth)):
            self.register_module(
                f"dec_{i}a", nn.Conv3d(c, c >> 1, ksize, 1, pad, bias=bias)
            )
            c = c >> 1
            self.register_module(
                f"dec_{i}b",
                nn.Conv3d(c, c >> 1 if i > 0 else c, ksize, 1, pad, bias=bias),
            )

        self.conv = nn.Conv3d(nch_feats, nch_out, ksize, 1, pad, bias=bias)
        self.max_pooling = nn.MaxPool3d(kernel_size=2)
        self.upsample = lambda x, y: F.interpolate(x, y.shape[2:], mode="nearest")

    def forward(
        self, input: torch.Tensor, target: torch.Tensor = None
    ) -> Union[Tuple[torch.Tensor, torch.Tensor], torch.Tensor]:
        """See :class:`src.model.base.BaseModel`"""
        x = input
        skip = []
        for i in range(self.n_depth):
            x = F.relu_(self.get_submodule(f"enc_{i}a")(x))
            x = F.relu_(self.get_submodule(f"enc_{i}b")(x))
            skip.append(x)
            x = self.max_pooling(x)

        x = F.relu_(self.get_submodule(f"enc_{self.n_depth}a")(x))
        x = F.relu_(self.get_submodule(f"enc_{self.n_depth}b")(x))

        for i in reversed(range(self.n_depth)):
            h = skip.pop()
            x = self.upsample(x, h)
            x = torch.cat([x, h], dim=1)
            x = F.relu_(self.get_submodule(f"dec_{i}a")(x))
            x = F.relu_(self.get_submodule(f"dec_{i}b")(x))

        output = self.conv(x)
        if self.residual:
            output = output + input

        if target is not None:
            return output, F.mse_loss(output, target)
        else:
            return output
