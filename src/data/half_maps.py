from collections.abc import Iterable
from typing import List, Tuple, Union

import mrcfile
import numpy as np

from .base import DATASET_REGISTRY, BaseDataset


@DATASET_REGISTRY.register()
class HalfMapsDataset(BaseDataset):
    """
    A :class:`BaseDataset` object containing pairs of slices sampled from the provided
    `half_maps`. For each half map, there will be `nb_slices * train_val_split` slices
    for training and `nb_slices * (1 - train_val_split)` slices for validation,
    depending on whether the `is_train` flag is set. The half maps will be normalized if
    `normalise` is set to True. The size of each slice is given by `patch_size`, which
    can be a tuple of three integers or a single integer that describes the side length
    of a cubic slice.
    """

    def __init__(
        self,
        *,
        half_maps: List[List[str]],
        nb_slices: int = 1500,
        train_val_split: float = 0.8,
        normalise: bool = True,
        patch_size: Union[Tuple[int, int, int], int] = 64,
        is_train: bool = True,
    ) -> BaseDataset:
        """
        :param List[List[str]] half_maps: A list of lists containing paths to half maps.
        :param int nb_slices: Number of slices to sample from each half map, defaults to
            1500.
        :param float train_val_split: Proportion of slices to use for training, defaults
            to 0.8.
        :param bool normalise: Whether to normalize the half maps, defaults to True.
        :param Union[Tuple[int, int, int], int] patch_size: Size of each slice; can be a
            tuple for a 3D slice or a single integer for a cubic slice, defaults to 64.
        :param bool is_train: Flag indicating if the dataset is for training (True) or
            validation (False), defaults to True.
        :return BaseDataset: The initialized Dataset object.
        """
        super().__init__()
        self.is_train = is_train

        assert all(
            map(lambda x: len(x) == 2, half_maps)
        ), "each entry in half maps must have exactly two files"
        # check patch size
        if isinstance(patch_size, Iterable):
            assert len(patch_size) == 3, f"expected 3 numbers, got {len(patch_size)}"
        else:
            assert isinstance(
                patch_size, int
            ), f"expected int or (int, int, int), got {type(patch_size)}"
            patch_size = (patch_size,) * 3

        # total number of slices
        num_slices_per_sample = int(nb_slices * train_val_split)
        if not is_train:
            num_slices_per_sample = nb_slices - num_slices_per_sample
        self.num_slices = len(half_maps) * num_slices_per_sample

        # read and sample slices from all half maps
        self.half_maps = np.empty([self.num_slices, 2, *patch_size], dtype=np.float32)
        offset = 0
        for x, y in half_maps:
            hm1 = np.array(mrcfile.read(x), dtype=np.float32, copy=True)
            hm2 = np.array(mrcfile.read(y), dtype=np.float32, copy=True)
            if normalise:
                t = np.stack([hm1, hm2])
                eps = np.finfo(t.dtype).eps
                hm1 = (hm1 - t.mean()) / max(t.std(), eps)
                hm2 = (hm2 - t.mean()) / max(t.std(), eps)
            row, col, sec = np.array(hm1.shape) - np.array(patch_size)
            coords = np.mgrid[:row, :col, :sec].astype(np.int32).reshape(3, -1).T
            coords = coords[
                np.random.choice(
                    len(coords),
                    size=num_slices_per_sample,
                    replace=len(coords) < num_slices_per_sample,
                ),
                :,
            ]
            for i in range(num_slices_per_sample):
                c = tuple(slice(i, i + s) for i, s in zip(coords[i], patch_size))
                self.half_maps[offset + i, 0] = hm1[c]
                self.half_maps[offset + i, 1] = hm2[c]
            offset += num_slices_per_sample

    def __len__(self) -> int:
        """
        :return int: The total number of slices
        """
        return self.num_slices

    def __getitem__(self, index: int) -> Tuple[np.ndarray, np.ndarray]:
        """
        Retrieves the pair of slices at the given `index`. For training samples, a
        random rotation is applied; otherwise, the slices are returned as they are.

        The slices have the shape (1, row, col, sec), where `row`, `col`, and `sec` are
        determined by the patch size.

        :param int index: The index of the selected pair of slices.
        :return Tuple[np.ndarray, np.ndarray]: The selected pair of slices.
        """
        x, y = self.half_maps[index]
        if np.random.random() < 0.5 and not self.is_train:
            k = np.random.randint(0, 4)
            x = np.rot90(x, k=k, axes=[0, 2])
            y = np.rot90(y, k=k, axes=[0, 2])
        x, y = x[np.newaxis], y[np.newaxis]
        return x.copy(), y.copy()
